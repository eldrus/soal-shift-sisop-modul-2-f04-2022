# soal-shift-sisop-modul-2-F04-2022
anggota kelompok :
- sayid ziyad ibrahim alaydrus
- rafiqi rachmat
- david Fischer

## soal 2
a. mengextract zip kedalam folder “/home/[user]/shift2/drakor”, serta program harus dapat menghapus folder-folder yang tidak penting.
b. membuat folder berdasarkan genre dari isi zip yang ada.
c. poster yang ada akan dipindahkan ke dalam folder dengan genre yang sama dan merename folder.
d. foto yang harus dipindahkan ke dalam folder dengan nama yang sesuai.
e. membuat file "data.txt" yang berisi nama dan tahun rilis drama korea dan kemudian disorting berdasarkan ascending.

```
#include <unistd.h>

int main (int argc,char *arg[], char* env[]){
    char *argv[] = {"/bin/bash", "-c", "jalur=/home/$USER/shift2/drakor && mkdir -p $jalur && unzip -o -d $jalur drakor.zip '*.png' && find $jalur -name '*_*' -printf '%f\n' | awk -F_ '{print $0,$1,$2}'| while read asli file1 file2; do cp -T $jalur/$asli $jalur/$file1.png; cp -T $jalur/$asli $jalur/$file2;rm $jalur/$asli;done && ls $jalur | awk -F '[;_]' '{ print $NF }' | awk -F '.' '{print $1}' | sort | uniq | while read genre; do mkdir -p $jalur/$genre; filegenre=*$genre.png; find $jalur -name $filegenre -exec mv -t $jalur/$genre {} +;echo kategori : $genre > $jalur/$genre/data.txt; done && find $jalur -name '*.png' | awk -F '[;/]' '{print $0, $6, $7, $8}' | while read asli genre nama tahun;do mv -f -T $asli $jalur/$genre/$nama.png;echo $tahun $nama >> $jalur/$genre/temp.txt; done && find $jalur -type d | awk -F[/] '{if ($6) print $6}' | while read genre; do cat $jalur/$genre/temp.txt | sort | while read tahun nama; do echo '\n'nama : $nama'\n'rilis : tahun $tahun >> $jalur/$genre/data.txt; rm -f $jalur/$genre/temp.txt ; done;done && echo 2", 0};
    execve(argv[0], &argv[0], env);
    return 0;
}
``` 
Program akan mengunzip folder untuk disimpan kedalam suatu folder yang dimana program akan menghapus file yang tidak penting, kemudian mengkategorikan poster drama korea berdasarkan jenis atau gendre ke dalam sebuah folder yang sudah dinamai ulang. File "data.txt" dibuat untuk menyimpan nama dan tahun rilis drama korea yang telah disorting.

berikut adalah hasilnya :
![](https://drive.google.com/file/d/1Fj_qhEK7tVzFZO0QKRiK27R48U29e3aI/view?usp=sharing)
![data film horror](https://drive.google.com/file/d/1IOreM4_wx8Kv2ku8AuOfwb9TGq3hJSNC/view?usp=sharing)
![data .txt](https://drive.google.com/file/d/1_96CA6c_gm7k2uF9sbhdg-OZfwQ1vNa_/view?usp=sharing)


## soal 3
a.) Conan diminta membuat program untuk membuat 2
directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian
membuat directory ke 2 dengan nama “air”.

``` 
int main (){

	makedirDarat();
	makedirAir();

}
``` 
1. di main kita akan akan memanggil fungsi makedirDarat() sebagai fungsi untuk membuat folder Darat lalu dilanjutkan dengan memanggil fungsi makedirAir.

```
void makedirDarat(){
	pid_t child_id;
	int status;
	
	child_id=fork();
	if (child_id<0){
		exit(EXIT_FAILURE);
}
	if (child_id ==0){
	char  *argv[]={"mkdir","-p", "modul2/darat", NULL};
	execv("/usr/bin/mkdir", argv);
	}	
``` 
```
void makedirAir(){
	pid_t child_id;
	int status;
	
	child_id=fork();
	if (child_id<0){
	exit(EXIT_FAILURE);
}
	sleep(3);
	if (child_id ==0){
	char  *argv[]={"mkdir", "-p", "modul2/air", NULL};
	execv("/usr/bin/mkdir", argv);
	}
		
	else {
	while((wait(&status)) >0);
	unzipDarat();
	unzipAir();
}	
``` 
diatas adalah program untuk membuat folder Darat dan air di dalam directori home/user/modul2. dengan menggunakan execv. lalu setelah folder air dibuat dilanjutkan memanggil fungsi unzip animal air dan darat.

b.) hasil extract dipisah menjadi hewan darat dan hewan air sesuai
dengan nama filenya. Untuk hewan darat dimasukkan ke folder
“/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder
“/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder
air adalah 3 detik dimana folder darat dibuat terlebih dahulu.

```
void unzipDarat(){
	pid_t child_id;
	int status;
	
	child_id = fork();
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
		char *argv[] = {"unzip", "animal.zip","*darat*", NULL};
		execv("/bin/unzip", argv);		
	}
``` 

```
	else if (child_id == 0){
		char *argv[] = {"unzip", "animal.zip","*air*", NULL};
		execv("/bin/unzip", argv);		
	}
```
program diatas untuk mengunzip file animal.zip
1. menchechek apakah child < 0, jika ia maka failed dan langsung exit 
2. kita mengunzip file animal.zip dengan isi yang berformat "darat" menggunakan execv dan "*darat*" untuk mengenali string nama file nya
3. setelah unzip file darat dilanjuti dengan unzip file yang berformat air dengan algoritma yang sama.

c.) hasil extract dipisah menjadi hewan darat dan hewan air sesuai
dengan nama filenya. Untuk hewan darat dimasukkan ke folder
“/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder
“/home/[USER]/modul2/air”
```
 while ((ep = readdir (dp))) {
      
          	if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
          	if(fork()==0) continue;
          	strcpy(pathSrc,pathAwal);
          	strcat(pathSrc,ep->d_name);
          	
          	char *argv[] = {"mv",pathSrc, "/home/ziyad/modul2/darat", NULL};
          	execv("/bin/mv", argv);	
``` 
setelah unzip file tadi , file tersebut kita  masukin sesuai namanya dengan menggunakan strcmp untuk membandingkan string lalu menggunakan strcat untuk membaca string belakang, jika string sama kita pakai "mv" untuk memindahkan file sesuai directorinya. 

d.) a pihak
kebun binatang harus merelakannya sehingga conan harus menghapus semua
burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung
ditandai dengan adanya “bird” pada nama file.
```
if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
        	  	strcpy(pathSrc,pathAwal);
        	  	strcat(pathSrc,ep->d_name);
          		if(strstr(pathSrc,"bird")) {
          			if(fork()==0) continue;
          			removeBird2(pathSrc);
```
```
void removeBird2(char *path){	
	pid_t child_id;
	child_id = fork();
	int status;
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
		char *argv[] = {"rm",path, NULL};
        	execv("/bin/rm", argv);
```
remove gambar yang memiliki string dengan membaca string dari path file lalu panggil fungsi removebird2 untuk remove gambar.

e.) Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan
membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air”
ke “list.txt” dengan format UID_[UID file permission]_Nama File

```
struct passwd *pw = getpwuid(info.st_uid);

    		if (pw != 0) strcat(hasil,pw->pw_name);
    		strcat(hasil,"_");
    		if( info.st_mode & S_IRUSR )
        		strcat(hasil,"r");
    		if( info.st_mode & S_IWUSR )
        		strcat(hasil,"w");
    		if( info.st_mode & S_IXUSR )
        		strcat(hasil,"x");
        		
        	strcat(hasil,"_");
        	strcat(hasil,ep->d_name);
        	//writelist(hasil);
        	fprintf(fPtr,"%s\n", hasil);
```

lalu membuat list.txt di directory air yang berisikan gambar" apa saja yang ada dalam directori tersebut.

berikut adalah hasil yang didapat 
![hasil darat](https://drive.google.com/file/d/1_gz7PzbTQuStOyKPS9oOcwG8ila2PkAi/view?usp=sharing)

![hasil air](https://drive.google.com/file/d/1kiPUYMRge1NAbP_Xx94sE30Y5fDVj8lj/view?usp=sharing)





