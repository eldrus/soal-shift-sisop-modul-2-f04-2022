#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>
#include <sys/stat.h>


void writeList(){
    FILE * fPtr;
    
    fPtr = fopen("/home/ziyad/modul2/air/list.txt", "w");

    if(fPtr == NULL)
    {
        printf("cannot create file list.\n");
        exit(EXIT_FAILURE);
    }
    
    DIR *dp;
    struct dirent *ep;
    char pathAwal[]="/home/ziyad/modul2/air/";
    char pathSrc[]="/home/ziyad/modul2/air/";
    char hasilAwal[]="";
    char hasil[]="";
    int r;
     struct stat info;
    
    dp = opendir(pathAwal);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
      
          	if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
          	strcpy(pathSrc,"/home/ziyad/modul2/air/");
          	strcat(pathSrc,ep->d_name);
          	
          	strcpy(hasil,hasilAwal);
          	r = stat(pathSrc, &info);
    		if( r==-1 )
    		{		
      			fprintf(stderr,"File error");
        		exit(1);
    		}

    		struct passwd *pw = getpwuid(info.st_uid);

    		if (pw != 0) strcat(hasil,pw->pw_name);
    		strcat(hasil,"_");
    		if( info.st_mode & S_IRUSR )
        		strcat(hasil,"r");
    		if( info.st_mode & S_IWUSR )
        		strcat(hasil,"w");
    		if( info.st_mode & S_IXUSR )
        		strcat(hasil,"x");
        		
        	strcat(hasil,"_");
        	strcat(hasil,ep->d_name);
        	//writelist(hasil);
        	fprintf(fPtr,"%s\n", hasil);
        }

      	(void) closedir (dp);
    } 
    
    fclose(fPtr);
}

void removeBird2(char *path){	
	pid_t child_id;
	child_id = fork();
	int status;
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
		char *argv[] = {"rm",path, NULL};
        	execv("/bin/rm", argv);
	}
	else{
		while((wait(&status)) > 0);
		writeList();
	}
        
        return;
}

void removeBird(){
	DIR *dp;
    	struct dirent *ep;
    	char pathAwal[]="/home/ziyad/modul2/darat/";
    	char pathSrc[]="/home/ziyad/modul2/darat/";

	pid_t child_id;
	child_id = fork();
	int status;
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
    	dp = opendir(pathAwal);

    	if (dp != NULL)
    	{
     	while ((ep = readdir (dp))) {
      	
        	  	if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
        	  	strcpy(pathSrc,pathAwal);
        	  	strcat(pathSrc,ep->d_name);
          		if(strstr(pathSrc,"bird")) {
          			if(fork()==0) continue;
          			removeBird2(pathSrc);
			}
        	  		
        }
        } 
    	}
	
}


void moveanimalDarat(){
    DIR *dp;
    struct dirent *ep;
    char pathAwal[]="/home/ziyad/animal/";
    char pathSrc[]="/home/ziyad/animal/";
    int status;
    
    dp = opendir(pathAwal);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
      
          	if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
          	if(fork()==0) continue;
          	strcpy(pathSrc,pathAwal);
          	strcat(pathSrc,ep->d_name);
          	
          	char *argv[] = {"mv",pathSrc, "/home/ziyad/modul2/darat", NULL};
          	execv("/bin/mv", argv);	
      }

    }
      
    return;
}

void moveanimalAir(){
    DIR *dp;
    struct dirent *ep;
    char pathAwal[]="/home/ziyad/animal/";
    char pathSrc[]="/home/ziyad/animal/";
    int status;
    
    pid_t child_id;
    
    child_id = fork();
    if (child_id < 0){
    	exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
    	dp = opendir(pathAwal);

    	if (dp != NULL)
    	{
     	while ((ep = readdir (dp))) {
      	
        	  	if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
        	  	if(fork()==0) continue;
        	  	strcpy(pathSrc,pathAwal);
        	  	strcat(pathSrc,ep->d_name);
          	
        	  	char *argv[] = {"mv",pathSrc, "/home/ziyad/modul2/air", NULL};
        	  	execv("/bin/mv", argv);	
        }
        (void) closedir (dp);
        } 
    }
    else{
    	while((wait(&status)) > 0);
    	removeBird();
    }
    
    return;
}

void unzipDarat(){
	pid_t child_id;
	int status;
	
	child_id = fork();
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
		char *argv[] = {"unzip", "animal.zip","*darat*", NULL};
		execv("/bin/unzip", argv);		
	}
	else{
		while((wait(&status)) > 0);
		moveanimalDarat();
	}
	return;
}

void unzipAir(){
	pid_t child_id;
	int status;
	
	child_id = fork();
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
		char *argv[] = {"unzip", "animal.zip","*air*", NULL};
		execv("/bin/unzip", argv);		
	}
	else{
		while((wait(&status)) > 0);
		moveanimalAir();
		
	}
	return;
}

void makedirDarat(){
	pid_t child_id;
	int status;
	
	child_id=fork();
	if (child_id<0){
		exit(EXIT_FAILURE);
}
	if (child_id ==0){
	char  *argv[]={"mkdir","-p", "modul2/darat", NULL};
	execv("/usr/bin/mkdir", argv);
	}	
	

}

void makedirAir(){
	pid_t child_id;
	int status;
	
	child_id=fork();
	if (child_id<0){
	exit(EXIT_FAILURE);
}
	sleep(3);
	if (child_id ==0){
	char  *argv[]={"mkdir", "-p", "modul2/air", NULL};
	execv("/usr/bin/mkdir", argv);
	}
		
	else {
	while((wait(&status)) >0);
	unzipDarat();
	unzipAir();
}	
		
}

int main (){

	makedirDarat();
	makedirAir();

}


